/**
 * FixFirstLogin - Stop players from dying when they first log in.
 * Copyright (C) 2012-2015 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package me.ryvix.FixFirstLogin;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class FixFirstLoginListener implements Listener {

	private final FixFirstLogin plugin;

	/**
	 * The listener
	 *
	 * @param plugin
	 */
	public FixFirstLoginListener(FixFirstLogin plugin) {
		this.plugin = plugin;
	}

	/**
	 * Run a command when a player joins
	 *
	 * @param event
	 */
	@EventHandler
	public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
		OfflinePlayer player = plugin.getServer().getOfflinePlayer(event.getUniqueId());

		// check prevent_new_logins
		if (plugin.getPrevent()) {
			if (!player.hasPlayedBefore()) {
				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, plugin.getPreventMsg());
			}
		}

	}

	/**
	 * Run a command when a player joins
	 *
	 * @param event
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Player p = event.getPlayer();

		long currentTime = System.currentTimeMillis();
		long firstPlayed = p.getFirstPlayed();
		long diff = currentTime - firstPlayed;

		// make sure player has been logged in for a span of time
		if (diff < plugin.getSpan()) {

			// run command after set number of ticks
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				@Override
				public void run() {
					if (p != null) {

						// replace PLAYER with the player's name
						String command = plugin.getCommand().replaceAll("PLAYERNAME", p.getName()).replaceAll("PLAYERUUID", p.getUniqueId().toString());

						System.out.println(p.getUniqueId().toString());

						switch (plugin.getRunas()) {
							case "player":
								p.performCommand(command);
								break;
							case "console":
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
								break;
							default:
								plugin.getLogger().log(Level.SEVERE, "You entered a wrong config value for runas! You entered: {0}. It must be either player or console.", plugin.getRunas());
								break;
						}
					}
				}
			}, plugin.getTicks());
		}
	}
}
