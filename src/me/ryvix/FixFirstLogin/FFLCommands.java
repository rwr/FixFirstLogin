/**
 * FixFirstLogin - Stop players from dying when they first log in.
 * Copyright (C) 2012-2015 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package me.ryvix.FixFirstLogin;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

class FFLCommands implements CommandExecutor {

	final FixFirstLogin plugin;

	public FFLCommands(FixFirstLogin plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		String cName = cmd.getName();

		if (cName.equalsIgnoreCase("ffl") && sender.hasPermission("ffl.prevent")) {
			if (args.length == 0) {
				sender.sendMessage("Not enough arguments!");
				return true;
			}

			if (args[0].equalsIgnoreCase("prevent")) {
				if (args[1].equalsIgnoreCase("on")) {
					plugin.setPrevent(true);
					plugin.getConfig().set("prevent_new_logins", true);
					plugin.saveConfig();
					sender.sendMessage("Now preventing new players from logging in.");
				} else if (args[1].equalsIgnoreCase("off")) {
					plugin.setPrevent(false);
					plugin.getConfig().set("prevent_new_logins", false);
					plugin.saveConfig();
					sender.sendMessage("Now allowing new players to log in.");
				}

			} else if (args[0].equalsIgnoreCase("reload")) {
				if (sender.hasPermission("ffl.reload")) {
					plugin.reload();
					sender.sendMessage(ChatColor.GREEN + "FixFirstLogin config reloaded!");
				} else {
					sender.sendMessage(ChatColor.RED + "You don't have permission to do that!");
				}

			} else {
				sender.sendMessage("Wrong arguments! /ffl prevent <on|off>");
				return true;
			}
		}
		return true;
	}
}
