/**
 * FixFirstLogin - Stop players from dying when they first log in.
 * Copyright (C) 2012-2015 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package me.ryvix.FixFirstLogin;

import java.util.logging.Level;
import org.bukkit.plugin.java.JavaPlugin;

public class FixFirstLogin extends JavaPlugin {

	private int ticks = 0;
	private String command = null;
	private String runas = null;
	private int span = 0;
	private boolean prevent = false;
	private String preventMsg;

	/**
	 * Runs when plugin is enabled
	 */
	@Override
	public void onEnable() {

		// Register the listener
		getServer().getPluginManager().registerEvents(new FixFirstLoginListener(this), this);

		try {
			this.saveDefaultConfig();
		} catch (Exception x) {
			getLogger().log(Level.SEVERE, x.getLocalizedMessage(), x);
		}
		setupConfig();
		setVariables();

		getCommand("ffl").setExecutor(new FFLCommands(this));

	}

	/**
	 * Runs when plugin is disabled
	 */
	@Override
	public void onDisable() {
		clearVariables();
	}

	public void reload() {
		reloadConfig();
		setupConfig();
		clearVariables();
		setVariables();
	}

	public void clearVariables() {
		ticks = 0;
		command = null;
		runas = null;
		span = 0;
		prevent = false;
		preventMsg = null;
	}

	/**
	 * Setup the config file.
	 */
	public void setupConfig() {
		this.getConfig().options().copyDefaults(true);
		this.getConfig().addDefault("ticks", 20);
		this.getConfig().addDefault("command", "spawn");
		this.getConfig().addDefault("runas", "player");
		this.getConfig().addDefault("span", 5000);
		this.getConfig().addDefault("prevent_new_logins", false);
		this.getConfig().addDefault("prevent_message", "New players are not currently allowed to login. Please try again later.");
		this.saveConfig();
	}

	/**
	 * Set config variables
	 */
	public void setVariables() {
		ticks = this.getConfig().getInt("ticks");
		command = this.getConfig().getString("command");
		runas = this.getConfig().getString("runas");
		span = this.getConfig().getInt("span");
		prevent = this.getConfig().getBoolean("prevent_new_logins");
		preventMsg = this.getConfig().getString("prevent_message");
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getRunas() {
		return runas;
	}

	public void setRunas(String runas) {
		this.runas = runas;
	}

	public int getSpan() {
		return span;
	}

	public void setSpan(int span) {
		this.span = span;
	}

	public boolean getPrevent() {
		return prevent;
	}

	public void setPrevent(boolean prevent) {
		this.prevent = prevent;
	}

	public void setPreventMsg(String msg) {
		this.preventMsg = msg;
	}

	String getPreventMsg() {
		return preventMsg;
	}
}
