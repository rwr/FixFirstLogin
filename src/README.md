FixFirstLogin
=============

A Bukkit plugin to stop players from dying when they first log in.

http://dev.bukkit.org/bukkit-plugins/fixfirstlogin/

I made this little plugin to fix new players getting stuck in walls or spawning on roofs, etc. and dying when they first log into the server.

It works by running a command after a configurable amount of ticks when players log in for the first time (or they don't have a player file).

I thought I should upload it here in case someone might find it useful. I couldn't find anything to do what I wanted so here it is.

config.yml
==========

The config.yml file should be automatically generated for you the first time you start your server with the plugin in your plugins folder. There are 2 variables which you can configure if you like.

ticks: 20
The number of ticks to wait before running the command

command: spawn
The command to run after waiting the number of ticks.

Note that if you use spawn as your command you should have a plugin that provides that command or it won't work.

Replacement variables

The command config option can contain variables that will be replaced with values when the command is run.

i.e. command: tell PLAYERNAME hey there

PLAYERNAME - the player's name.
PLAYERUUID - the player's UUID.

runas: player
Run the command as the player or as console. Default player

span: 5000
The number of milliseconds the player must have been logged in to avoid the command being run when they join. Default 5000

prevent_new_logins: false
Stops new players from logging in.

prevent_message: "New players are not currently allowed to login. Please try again later."
The message to display to players who are prevented from logging in.

Commands
========
/ffl prevent <on|off>
Turn on or off the preventing of new players from logging in.

Permissions
===========
ffl.prevent
Gives access to use /ffl prevent <on|off>

ffl.*
Gives all FixFirstLogin permissions. (currently useless)